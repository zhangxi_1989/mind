//
//  Node.swift
//  Mind
//
//  Created by zhangxi on 26/12/2016.
//  Copyright © 2016 zhangxi. All rights reserved.
//

import UIKit
import Chameleon
import ZXTools

protocol NodeDelegate {
    func addChild(node:Node)
}


class Node: UIView {


    var delegate:NodeDelegate?
    var title:String?
    var children = [Node]()
    var selected:Bool = true
        {didSet{
            self.setNeedsDisplay()
            button.isHidden = !selected
        }
    }
    
    let button = UIButton(type:.custom )
    var titleLabel:UILabel!
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.selected = !self.selected
//    }
//    
    
    convenience init(center:CGPoint) {
        self.init(frame:CGRect(x: center.x-50, y: center.y-22, width: 130, height: 44))
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.isUserInteractionEnabled = true
        self.backgroundColor = UIColor.clear
       
        
        
        
        let w = frame.size.width - 40
        let h = frame.size.height
        
        
        
        
        button.setImage(UIImage(named:"add"), for: .normal)
        button.frame  = CGRect(x: w, y: 2, width: 40, height: 40)
        button.addTarget(self, action: #selector(Node.add(button:)), for: .touchUpInside)
        button.isHidden = !selected
        button.isUserInteractionEnabled = true
        self.addSubview(button)
        
        
        titleLabel = UILabel(frame: CGRect(x:0,y:0,width:w,height:h))
        titleLabel.text = "Node"
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor.flatSkyBlueColorDark()
        self.addSubview(titleLabel)
 

    }
    
    func add(button:UIButton)
    {
        delegate?.addChild(node: self)
    }
    

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        var border = [CGPoint]()
        
        let w = self.frame.size.width - 40
        let h = self.frame.size.height
        
        if selected
        {
            border.append(CGPoint(x: 0,  y: 0))
            border.append(CGPoint(x: 0,  y: h))
            border.append(CGPoint(x: w,  y: h))
            border.append(CGPoint(x: w,  y: 0))
            border.append(CGPoint(x: 0,  y: 0))
            
            
            //button.draw(CGRect(x: w, y: 2, width: 40, height: 40))
        }else
        {
            border.append(CGPoint(x: 0,  y: h))
            border.append(CGPoint(x: w,  y: h))
        }
        
        //titleLabel.draw(CGRect(x: w, y: 2, width: 40, height: 40))
        
        UIColor.clear.setFill()
        UIColor.flatSkyBlue().setStroke()
        self.drawLines(border)
    }
    
    
}
