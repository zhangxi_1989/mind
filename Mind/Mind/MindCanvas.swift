//
//  MindCanvas.swift
//  Mind
//
//  Created by zhangxi on 27/12/2016.
//  Copyright © 2016 zhangxi. All rights reserved.
//

import UIKit
import ZXTools


class MindCanvas: UIView {

    
    var root:Node!
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        
        
        for node in root.children
        {
            drawLines([root.center,node.center])
        }
        
        
    }
 
 

}
