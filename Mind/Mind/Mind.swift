//
//  Mind.swift
//  Mind
//
//  Created by zhangxi on 26/12/2016.
//  Copyright © 2016 zhangxi. All rights reserved.
//

import UIKit
import Chameleon

class Mind: UIViewController,NodeDelegate{

    @IBOutlet weak var canvas     : MindCanvas!
    
    
    var root:Node!
    var nodes = [Node]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        

        
        canvas.backgroundColor = UIColor.flatLime()
        
        
        
        root = Node(center: CGPoint(x: self.view.bounds.width/2, y: self.view.bounds.height/2))
        root.delegate = self
        self.canvas.addSubview(root)
        self.canvas.root = root
        self.canvas.setNeedsDisplay()
        
        
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(Mind.pinch(gesture:)))
        self.view.addGestureRecognizer(pinch)
        
        let move = UIPanGestureRecognizer(target: self, action: #selector(Mind.move(gesture:)))
        self.view.addGestureRecognizer(move)
        
        let long = UILongPressGestureRecognizer(target: self, action: #selector(Mind.long(gesture:)))
        self.view.addGestureRecognizer(long)
    
        
        nodes.append(root)
    }

    
    var detalPosition:CGPoint?
    var canvasSize:CGSize?
    var scale:CGFloat = 1
    
    func pinch(gesture:UIPinchGestureRecognizer)
    {
        let positon = gesture.location(in: self.view)
        print(positon)
        
        
        switch gesture.state
        {
        case .began:
            break
        case .changed:
            canvas.transform = CGAffineTransform(scaleX: scale * gesture.scale, y: scale * gesture.scale)
        case .ended:
            
            scale = scale * gesture.scale
            
        default:
            break
        }
    }
    
    func move(gesture:UIPanGestureRecognizer)
    {
        let point = gesture.translation(in: self.view)
        
        switch gesture.state
        {
        case .began:break
        case .changed:
            self.canvas.center = CGPoint(x:self.canvas.center.x + point.x , y:self.canvas.center.y + point.y)
        case .ended,.cancelled,.failed:break
        default:
            break
        }
        gesture.setTranslation(CGPoint.zero, in: self.view)
    }
    
    func long(gesture:UILongPressGestureRecognizer)
    {
        scale = 1
        canvas.transform = CGAffineTransform.identity
    }

    
    
    
    func addChild(node: Node) {
        
        scale = 1
        canvas.transform = CGAffineTransform.identity
        
        
        let center = CGPoint(x:node.center.x + node.frame.size.width ,y:node.center.y)
        
        
        let child = Node(center: center)
        child.delegate = self
        self.canvas.addSubview(child)
        
        nodes.append(child)
        
        node.children.append(child)
        
        
        self.canvas.frame = findEdge()
        self.canvas.setNeedsDisplay()
    }
    
    
    
    
    func findEdge()->CGRect
    {
        var xMax = 0.0 as CGFloat
        var yMax = 0.0 as CGFloat
        
        for node in self.canvas.subviews
        {
            xMax = max(xMax  , node.frame.origin.x + node.frame.size.width)
            yMax = max(yMax  , node.frame.origin.y + node.frame.size.height)
        }

        var rect = CGRect(x:self.canvas.frame.origin.x , y:self.canvas.frame.origin.y , width:xMax - self.canvas.frame.origin.x , height:yMax - self.canvas.frame.origin.y)
        
        
        rect.size.width  = max(rect.size.width  , UIScreen.main.bounds.size.width) + 100
        rect.size.height = max(rect.size.height , UIScreen.main.bounds.size.height) + 100
        
        
        return rect
    }



}






