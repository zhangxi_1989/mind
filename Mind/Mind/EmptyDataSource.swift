//
//  EmptyDataSource.swift
//  Bandwagon
//
//  Created by zhangxi on 25/12/2016.
//  Copyright © 2016 zhangxi.me. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class EmptyDataSource: NSObject,DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {

    func bind(scrollView:UIScrollView)
    {
        scrollView.emptyDataSetSource = self
    }
    
    
    //MARK: empty data set
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named:"empty")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let tip = "No Mind"
        return NSAttributedString(string: tip)
    }
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let tip = "Please add mind by tap plus button on the right-top corner."
        return NSAttributedString(string: tip)
    }

}
